package com.jaga888;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtils {
    public String getContentFile(String inputPath) throws Exception {
        try (BufferedReader reader = new BufferedReader(new FileReader(inputPath))) {
            String line;
            StringBuilder stringBuilder = new StringBuilder();
            String lineSeparator = System.getProperty("line.separator");
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(lineSeparator);
            }
            return stringBuilder.toString();
        }
    }

    public void saveFile(String outputPath, String output) throws IOException {
        try (FileWriter writer = new FileWriter(outputPath, false)) {
            writer.write(output);
        }
    }
}
