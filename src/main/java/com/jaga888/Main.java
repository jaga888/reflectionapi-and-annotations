package com.jaga888;

import com.jaga888.classes.test.HacckerAttackFields;

public class Main {
    public static void main(String[] args) throws Exception {
        ReflectionUtils reflectionUtils = new ReflectionUtils();
        HacckerAttackFields hacckerAttackFields = new HacckerAttackFields();
        reflectionUtils.hacckerAttack(hacckerAttackFields);

        ReflectionService reflectionService = new ReflectionService(reflectionUtils, new FileUtils());
        reflectionService.process();
    }
}
