package com.jaga888.classes.test;

import com.jaga888.annotations.Container;

@Container
@Container.FilePath
public class ClassTest3 {

    @Container.Transformation
    public String transformation(String text) {
        return text.toLowerCase();
    }
}
