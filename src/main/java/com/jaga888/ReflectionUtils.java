package com.jaga888;

import com.jaga888.annotations.Container;
import com.jaga888.annotations.HacckerAttack;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


public class ReflectionUtils {

    public Set<Class<?>> getAllClasses(String name) {
        List<ClassLoader> classLoaders = new LinkedList<>();
        classLoaders.add(ClasspathHelper.contextClassLoader());
        classLoaders.add(ClasspathHelper.staticClassLoader());
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoaders.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(name))));

        Set<Class<?>> allClasses = reflections.getSubTypesOf(Object.class);
        return allClasses;
    }

    public Set<Class<?>> getClassesWithAnnotation(Set<Class<?>> classes) {
        Set<Class<?>> classesWithAnnotation = new HashSet<>();
        for (Class clazz : classes) {
            if (clazz.isAnnotationPresent(Container.class)) {
                classesWithAnnotation.add(clazz);
            }
        }
        return classesWithAnnotation;
    }

    public Set<Method> getMethods(Set<Class<?>> classesWithAnnotation) {
        Set<Method> methodsWithAnnotation = new HashSet<>();
        for (Class clazz : classesWithAnnotation) {
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                if (method.isAnnotationPresent(Container.Transformation.class)) {
                    methodsWithAnnotation.add(method);
                }
            }
        }
        return methodsWithAnnotation;
    }

    public void hacckerAttack(Object clazz) throws IllegalAccessException {
        Field[] field = clazz.getClass().getDeclaredFields();
        for (Field temp : field) {
            temp.setAccessible(true);
            Object name = temp.get(clazz);
            Annotation[] annotations = temp.getDeclaredAnnotations();

            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(HacckerAttack.class)) {
                    if (name instanceof String) {
                        name = "hacker attack";
                    } else if (name instanceof Integer) {
                        name = 0;
                    }
                    System.out.println(temp.getType() + " : " + temp.getName() + " : " + name);
                }
            }
        }
    }
}
