package com.jaga888;

import com.jaga888.annotations.Container;

import java.lang.reflect.Method;
import java.util.Set;

public class ReflectionService {
    private String inputPath;
    private String outputPath;
    private String readFile;
    private String output;
    private Class processClass;
    private Container.FilePath filePath;
    private ReflectionUtils reflectionUtils;
    private FileUtils fileUtils;

    public ReflectionService(ReflectionUtils reflectionUtils, FileUtils fileUtils) {
        this.reflectionUtils = reflectionUtils;
        this.fileUtils = fileUtils;
    }

    public void process() throws Exception {
        Set<Class<?>> allClasses = reflectionUtils.getAllClasses("com.jaga888");
        Set<Class<?>> classesWithAnnotation = reflectionUtils.getClassesWithAnnotation(allClasses);
        Set<Method> methodsWithAnnotation = reflectionUtils.getMethods(classesWithAnnotation);

        for (Method method : methodsWithAnnotation) {
            processClass = method.getDeclaringClass();
            filePath = method.getDeclaringClass().getAnnotation(Container.FilePath.class);
            inputPath = filePath.inputPath();
            outputPath = filePath.outputPath();
            readFile = fileUtils.getContentFile(inputPath);
            output = (String) method.invoke(processClass.newInstance(), readFile);
            fileUtils.saveFile(outputPath, output);
        }
    }
}
