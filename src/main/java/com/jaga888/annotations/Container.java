package com.jaga888.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Container {

    @Retention(RetentionPolicy.RUNTIME)
    @interface FilePath {
        String inputPath() default "input.txt";

        String outputPath() default "output.txt";
    }

    @Retention(RetentionPolicy.RUNTIME)
    @interface Transformation {
    }
}

