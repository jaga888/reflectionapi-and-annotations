package com.jaga888.test.reflection;

import com.jaga888.classes.test.ClassTest3;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClassTest3Test {

    @Test
    void transformation() {
        ClassTest3 classTest3 = new ClassTest3();
        String test = classTest3.transformation("Fsfs..s.dfSFAd.f     SSSSS     ...");
        assertEquals(test, "fsfs..s.dfsfad.f     sssss     ...");
    }
}
