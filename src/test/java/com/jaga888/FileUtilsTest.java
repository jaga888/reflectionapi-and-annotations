package com.jaga888;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FileUtilsTest {

    @Test
    void getContentFile() throws Exception {

        FileUtils fileUtils = new FileUtils();
        String example = fileUtils.getContentFile("src\\test\\resources\\input.txt");
        System.out.println(example);
        assertEquals(example, String.format("%s%ndf@$gSSS.%n", "fas ASFasf asd=юююююююююsgds"));
    }

    @Test
    void fileIsNotExist() {
        FileUtils fileUtils = new FileUtils();
        assertThrows(FileNotFoundException.class,
                () -> {
                    fileUtils.getContentFile("notExist.txt");
                });
    }

    @Test
    void saveFile() throws Exception {
        FileUtils fileUtils = new FileUtils();
        fileUtils.saveFile("src\\test\\resources\\output.txt", "textsss\nsss");
        String example = fileUtils.getContentFile("src\\test\\resources\\output.txt");
        assertEquals(example, String.format("textsss%nsss%n"));
    }
}