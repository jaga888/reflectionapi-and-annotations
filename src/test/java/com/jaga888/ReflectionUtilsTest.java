package com.jaga888;

import com.google.common.collect.Sets;
import com.jaga888.test.reflection.ClassTest3Test;
import com.jaga888.test.reflection.ClassTest4Test;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ReflectionUtilsTest {

    @Test
    void getAllClasses() {
        ReflectionUtils reflectionUtils = new ReflectionUtils();
        Set<Class<?>> setClasses = reflectionUtils.getAllClasses("com.jaga888.test.reflection");
        Set<Class<?>> arr = Sets.newHashSet(ClassTest3Test.class, ClassTest4Test.class);
        assertEquals(setClasses, arr);
    }
}