package com.jaga888.classes.test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ClassTest2Test {

    @Test
    void transformation() {
        ClassTest2 classTest2 = new ClassTest2();
        String test = classTest2.transformation("Fsfs..s.dfSFAd.f          ...");
        assertEquals(test, "FSFS..S.DFSFAD.F          ...");

    }
}