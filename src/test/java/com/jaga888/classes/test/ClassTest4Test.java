package com.jaga888.classes.test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClassTest4Test {

    @Test
    void transformation() {
        ClassTest4 classTest4 = new ClassTest4();
        String test = classTest4.transformation("Fsfs .s dfSFAd.f    s  s a   ...");
        assertEquals(test, "Fsfs .s DfSFAd.f    S  S A   ...");
    }
}