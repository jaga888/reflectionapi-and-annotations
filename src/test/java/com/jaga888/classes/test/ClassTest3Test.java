package com.jaga888.classes.test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClassTest3Test {

    @Test
    void transformation() {
        ClassTest3 classTest3 = new ClassTest3();
        String test = classTest3.transformation("Fsfs..s.dfSFAd.f     SSSSS     ...");
        assertEquals(test, "fsfs..s.dfsfad.f     sssss     ...");
    }
}
