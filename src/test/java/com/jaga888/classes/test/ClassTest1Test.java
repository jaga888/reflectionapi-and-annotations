package com.jaga888.classes.test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClassTest1Test {

    @Test
    void transformation() {
        ClassTest1 classTest1 = new ClassTest1();
        String test = classTest1.transformation("Fsfs..s.dfSFAd.f          ...");
        assertEquals(test, "Fsfs--s-dfSFAd-f          ---");
    }
}